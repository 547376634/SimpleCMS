<?php
/**
 * 模板获取图片函数
 * @param array $args (str:Mod Controller Action param:条件参数)
 * @param object $smarty
 */
function smarty_function_Img($args,&$smarty)
{
	if(is_numeric($args['id'])){
		$id = $args['id'];
	}else{
		$tmp = array();
		$tmp = str_replace('$', '', $args['id']);
		$tmp = explode('.', $tmp);
		$data = $smarty->_tpl_vars;
		foreach($tmp as $v){
			$data =$data[$v];//迭代出数值
		}
		$id = $data;
	}
	
	$prefix = $args['prefix'];
	
	
	$data = M('file')->where('id='.$id)->getOne();
	$file = $data['folder'].$prefix.$data['savename'];
	
	if(file_exists($file)){
		echo $file;
	}else{
		echo $data['folder'].$data['savename'];
	}
	
}