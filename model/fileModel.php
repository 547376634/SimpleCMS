<?php


class fileModel extends Model{
	
	
	private $table = 'file';
	
	public function __construct(){
		parent::__construct($this->table);
	}
	static $_instance; //存储对象
	
	private function __clone(){}
	
	public static function getInstance($table){
		$model_name = $table.'Model';
		if(FALSE == (self::$_instance instanceof self)){
			self::$_instance = new self($table);
		}
		return self::$_instance;
	}
	
	/**
	 * 保存fileinfo到数据库中
	 * @param array $file [file.class.php上传文件后的得到的fileinfo]
	 * @return boolean
	 */
	public function saveFileToDB(&$file){
		$data = array();
		
		$data['filename'] = $file[0]['name'];
		$data['savename'] = $file[0]['savename'];
		$data['type'] = $file[0]['extension'];
		$data['size'] = $file[0]['size'];
		$data['folder'] = $file[0]['savepath'];
		$data['createtime'] = $data['updatetime'] = time();
		$data['uid'] = session('adminid');
		$id = $this->insert($data);
		$file[0]['id'] = $id;
		
// 		session('_upload',$file[0]);
		
		$_upload = session('_upload_cache');
		$_upload = json_decode($_upload,true);
		
		if($id){
			$data['id'] = $id;//file id
			$_upload[$id] = $data;
			session('_upload_cache',json_encode($_upload));
			return true;
		}else{
			return false;
		}
		
	}
	
	public function getPicList($ids){
// 		$ids = explode(',', $ids);
		$picList = M('file')->where('id IN ('.$ids.')')->getAll();
// 		debug($picList);
		$arr = array();
		foreach($picList as $v){
			$arr[$v['id']] = $v;
		}
// 		unset($picList);
		return $arr;
	}
}
?>