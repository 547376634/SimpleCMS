<?php


class productModel extends Model{


	private $table = 'product';

	public function __construct(){
		parent::__construct($this->table);
	}
	
	
	public function getList($page,$size,$cid){
		
		$data = M('product')->where('cateid = '.$cid)->order('id DESC')->page($page)->getAll();
		foreach($data as &$v){
			$v['pics'] = D('file')->getPicList($v['pics']);
		}
		$page = M('product')->getPager($page, U('default/product/clist'));
		
		return array('data'=>$data,'page'=>$page);
	}
}