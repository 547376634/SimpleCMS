set charset utf8;
DROP TABLE IF EXISTS `cx_admin` ;
CREATE TABLE `cx_admin` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `username` varchar(50) NOT NULL COMMENT '管理员名称',
  `password` varchar(32) NOT NULL COMMENT '管理员密码',
  `roleid` int(1) unsigned NOT NULL COMMENT '管理员角色',
  `ipaddress` varchar(30) DEFAULT NULL COMMENT '登录IP',
  `lastlogintime` int(10) unsigned DEFAULT NULL COMMENT '最后登录时间',
  `logincount` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员表';
insert into `cx_admin`(`id`,`username`,`password`,`roleid`,`ipaddress`,`lastlogintime`,`logincount`) values('1','admin','e10adc3949ba59abbe56e057f20f883e','1','127.0.0.1','1378361082','2');
DROP TABLE IF EXISTS `cx_backup` ;
CREATE TABLE `cx_backup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `path` varchar(150) DEFAULT '' COMMENT '路径',
  `createtime` int(11) DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
insert into `cx_backup`(`id`,`name`,`path`,`createtime`) values('1','db_cxnet2014122440.sql','/static/backup/db_cxnet2014122440.sql','1417509630');
DROP TABLE IF EXISTS `cx_category` ;
CREATE TABLE `cx_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `parentid` int(11) DEFAULT '0' COMMENT '父类id',
  `categoryname` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `type` enum('news','product','link','job','show') DEFAULT NULL COMMENT '类型',
  `issystem` tinyint(1) DEFAULT '0' COMMENT '系统集成(1:是,0:否)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('1','0','新闻','news','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('2','1','行业动态','news','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('3','1','企业新闻','news','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('4','0','招聘','job','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('5','4','社会招聘','job','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('6','4','校园招聘','job','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('7','0','产品','product','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('8','7','公司产品','product','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('9','0','链接','link','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('10','9','首页链接','link','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('11','9','内页链接','link','1');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('12','15','网站案例','show','0');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('13','15','软件案例','show','0');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('14','1','技术文章','news','0');
insert into `cx_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values('15','0','图集','show','0');
DROP TABLE IF EXISTS `cx_file` ;
CREATE TABLE `cx_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `filename` varchar(300) NOT NULL COMMENT '文件名称',
  `savename` varchar(300) DEFAULT NULL COMMENT '保存后的名称',
  `type` char(10) DEFAULT NULL COMMENT '文件类型',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `folder` char(25) DEFAULT NULL COMMENT '文件路径',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('1','1youku原型图规范.png','52ca65df24234.png','png','113752','static/uploads/20140106/','1388996063','1388996063','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('5','1369818448.jpg','52ca6b42c798e.jpg','jpg','63831','static/uploads/20140106/','1388997442','1388997442','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('3','1369817978.jpg','52ca6b31df761.jpg','jpg','39413','static/uploads/20140106/','1388997426','1388997426','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('4','1369817618.jpg','52ca6b36a3742.jpg','jpg','60008','static/uploads/20140106/','1388997430','1388997430','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('8','20130529_162654.gif','52ccb24e087ca.gif','gif','26935','static/uploads/20140108/','1389146702','1389146702','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('7','20130529_163800.gif','52cbbed45da20.gif','gif','33990','static/uploads/20140107/','1389084372','1389084372','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('9','20130529_163800.gif','52ccb24f59881.gif','gif','33990','static/uploads/20140108/','1389146703','1389146703','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('10','20130530_110834.jpg','52ccb25078234.jpg','jpg','73039','static/uploads/20140108/','1389146704','1389146704','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('11','1371095676.jpg','52ccb26c5be3a.jpg','jpg','21023','static/uploads/20140108/','1389146732','1389146732','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('12','1371095745.jpg','52ccb2c2228d2.jpg','jpg','13439','static/uploads/20140108/','1389146818','1389146818','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('13','20130529_163533.jpg','52ccb63407f71.jpg','jpg','20591','static/uploads/20140108/','1389147700','1389147700','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('14','20130529_162654.gif','52ccb8d68787e.gif','gif','26935','20140108/','1389148374','1389148374','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('15','20130529_162507.jpg','52ccbc8a4a5fb.jpg','jpg','48720','static/uploads/20140108/','1389149322','1389149322','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('19','易王大牌档.png','52d89c2dd6c24.png','png','939807','static/uploads/20140117/','1389927470','1389927470','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('18','564d994f7cd51cc89c6f9cf9bf26cd0b.png','52d3437a9d27a.png','png','300331','static/uploads/20140113/','1389577082','1389577082','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('20','mazhiquan.png','52d89cde2a7e0.png','png','1060964','static/uploads/20140117/','1389927646','1389927646','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('21','admin.png','52d8a49c280ac.png','png','25085','static/uploads/20140117/','1389929628','1389929628','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('22','index.png','52d8a49c739e6.png','png','50318','static/uploads/20140117/','1389929628','1389929628','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('23','wxshop.png','52d8ee5d798dd.png','png','120675','static/uploads/20140117/','1389948509','1389948509','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('24','后台.png','52d8ee6c91c6c.png','png','280169','static/uploads/20140117/','1389948524','1389948524','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('25','车小二1.jpg','52da65067ed93.jpg','jpg','165630','static/uploads/20140118/','1390044422','1390044422','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('26','图片1.jpg','52e219ddd63d3.jpg','jpg','146192','static/uploads/20140124/','1390549470','1390549470','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('27','迈博.png','52e21a29e56cd.png','png','735175','static/uploads/20140124/','1390549546','1390549546','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('28','麦迪.png','52e2284f72f30.png','png','287644','static/uploads/20140124/','1390553167','1390553167','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('29','麦迪2.png','52e2285648d97.png','png','132372','static/uploads/20140124/','1390553174','1390553174','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('30','白绒山羊1.png','52e22a8eeb3d4.png','png','1390160','static/uploads/20140124/','1390553743','1390553743','1');
insert into `cx_file`(`id`,`filename`,`savename`,`type`,`size`,`folder`,`createtime`,`updatetime`,`uid`) values('31','白绒山羊2.png','52e22aa59c154.png','png','675485','static/uploads/20140124/','1390553765','1390553765','1');
DROP TABLE IF EXISTS `cx_guestbook` ;
CREATE TABLE `cx_guestbook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(30) DEFAULT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(30) DEFAULT NULL COMMENT '电话',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型(0:主贴,1:回复)',
  `parentid` int(11) DEFAULT NULL COMMENT '主贴id',
  `content` text COMMENT '内容',
  `ipaddress` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `cx_job` ;
CREATE TABLE `cx_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) DEFAULT '' COMMENT '招聘名称',
  `cateid` int(3) DEFAULT NULL COMMENT '招聘类型(1:社会招聘,2:校园招聘)',
  `nums` int(5) NOT NULL DEFAULT '1' COMMENT '招聘人数',
  `jobdes` text COMMENT '岗位描述',
  `address` varchar(130) DEFAULT NULL COMMENT '工作地点',
  `email` varchar(50) DEFAULT NULL COMMENT '联系邮箱',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系手机',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
insert into `cx_job`(`id`,`name`,`cateid`,`nums`,`jobdes`,`address`,`email`,`tel`,`phone`,`createtime`,`updatetime`) values('1','设计师','5','5','赵超v啊别v啊&amp;nbsp;','西安','547376634@qq.com','18991335736','18991335736','1389151593','1389151593');
insert into `cx_job`(`id`,`name`,`cateid`,`nums`,`jobdes`,`address`,`email`,`tel`,`phone`,`createtime`,`updatetime`) values('2','业务员','6','1','阿斯顿v啊撒大声地对','渭南','547376634@qq.com','18991335736','18991335736','1389151615','1389151615');
DROP TABLE IF EXISTS `cx_link` ;
CREATE TABLE `cx_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(11) DEFAULT NULL COMMENT '分类id',
  `linkname` varchar(50) DEFAULT NULL COMMENT '连接名称',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型1:文字2:图片',
  `linkurl` varchar(150) DEFAULT NULL COMMENT '连接地址',
  `logourl` varchar(200) DEFAULT NULL COMMENT 'logo地址',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `uid` int(11) DEFAULT NULL COMMENT '管理员ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('1','10','技术人生','1','http://blog.cxiangnet.cn','','0','1388395742','1389528503','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('2','10','西安灏瀚广告','1','http://www.xahaohan.com/','','0','1389085670','1389332846','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('4','10','印象陕西','1','http://www.sxfeel.com','','0','1390728040','1390728040','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('5','10','乐活网','1','http://www.leohuo.com','','0','1390728060','1390728060','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('6','10','壹游酷','1','http://www.1youku.com','','0','1390728084','1390728084','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('7','10','众创科技','1','http://www.fpzckj.com','','0','1390728136','1390728136','1','0');
insert into `cx_link`(`id`,`cateid`,`linkname`,`type`,`linkurl`,`logourl`,`sort`,`createtime`,`updatetime`,`click`,`uid`) values('8','10','重庆大排档','1','http://www.ywcqdpd.com','','0','1390728168','1390728168','1','0');
DROP TABLE IF EXISTS `cx_mod` ;
CREATE TABLE `cx_mod` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) DEFAULT '' COMMENT '模块名称',
  `mod` varchar(20) NOT NULL DEFAULT '' COMMENT '模块',
  `issystem` tinyint(1) DEFAULT '1' COMMENT '是否系统模块',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('1','新闻','news','1','1387421040','1387421040');
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('2','产品','product','1','1387421040','1387421040');
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('3','留言','guestbook','1','1387421040','1387421040');
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('4','招聘','job','1','1387421040','1387421040');
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('5','友链','link','1','1387421040','1387421040');
insert into `cx_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values('6','图集','show','1','0','0');
DROP TABLE IF EXISTS `cx_nav` ;
CREATE TABLE `cx_nav` (
  `name` varchar(30) NOT NULL COMMENT '名称',
  `key` varchar(30) NOT NULL COMMENT '键(header:头部导航,footer:底部导航)',
  `value` text COMMENT '值',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
insert into `cx_nav`(`name`,`key`,`value`,`updatetime`) values('头部导航','header','a:6:{i:0;a:3:{s:4:\"type\";s:6:\"system\";s:2:\"id\";s:1:\"0\";s:4:\"name\";s:6:\"首页\";}i:1;a:3:{s:4:\"type\";s:4:\"news\";s:2:\"id\";s:1:\"3\";s:4:\"name\";s:12:\"企业新闻\";}i:2;a:3:{s:4:\"type\";s:4:\"show\";s:2:\"id\";s:2:\"12\";s:4:\"name\";s:12:\"网站案例\";}i:3;a:3:{s:4:\"type\";s:4:\"page\";s:2:\"id\";s:1:\"2\";s:4:\"name\";s:12:\"联系我们\";}i:4;a:3:{s:4:\"type\";s:4:\"link\";s:2:\"id\";s:2:\"10\";s:4:\"name\";s:12:\"友情链接\";}i:5;a:3:{s:4:\"type\";s:9:\"guestbook\";s:2:\"id\";s:1:\"0\";s:4:\"name\";s:9:\"留言板\";}}','1413796917');
DROP TABLE IF EXISTS `cx_news` ;
CREATE TABLE `cx_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(11) NOT NULL COMMENT '分类id',
  `title` varchar(300) NOT NULL COMMENT '标题',
  `keywords` varchar(30) DEFAULT NULL COMMENT '关键词',
  `description` varchar(500) DEFAULT NULL COMMENT '描述',
  `content` text COMMENT '内容',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
insert into `cx_news`(`id`,`cateid`,`title`,`keywords`,`description`,`content`,`click`,`createtime`,`updatetime`,`uid`) values('1','3','创想网络与富平在线发展合作关系','创想网络,富平在线','','&lt;p&gt;
	创想网络与富平在线发展合作关系
&lt;/p&gt;
&lt;p&gt;
	www.0913ss.com
&lt;/p&gt;
&lt;p&gt;
	&lt;img src=&quot;/static/uploads/20140114/20140114170235_93604.png&quot; alt=&quot;&quot; /&gt;
&lt;/p&gt;','1','1389690171','1389690171','0');
insert into `cx_news`(`id`,`cateid`,`title`,`keywords`,`description`,`content`,`click`,`createtime`,`updatetime`,`uid`) values('2','3','工信部：手机预装软件下月起须报备审核','','工信部：手机预装软件下月起须报备审核','&lt;div align=&quot;left&quot; style=&quot;color:#333333;font-family:verdana, arial, tahoma;font-size:14px;background-color:#FFFFFF;&quot;&gt;
	&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 近日，工信部今年4月发布的《关于加强移动智能终端管理的通知》将于11月1日起实施，通知要求获得进网许可的手机预装软件应当向工信部报备审核。
&lt;/div&gt;
&lt;p align=&quot;left&quot; style=&quot;margin-left:10px;font-family:verdana, arial, tahoma;font-size:14px;color:#333333;text-indent:2em;background-color:#FFFFFF;&quot;&gt;
	　通知还要求不得预装未经用户同意，擅自收集、修改用户个人信息的软件以及擅自调用终端通信功能，造成流量消耗、费用损失、信息泄露的软件。
&lt;/p&gt;
&lt;p align=&quot;left&quot; style=&quot;margin-left:10px;font-family:verdana, arial, tahoma;font-size:14px;color:#333333;text-indent:2em;background-color:#FFFFFF;&quot;&gt;
	　近日，有多家媒体曝光了手机预装软件的黑幕。据悉，搭载安卓系统的智能手机预装软件最高达到二三十款，且用户在没有ROOT权限的情况下无法删除卸载，而一旦获得了ROOT权限手机将失去保修。
&lt;/p&gt;
&lt;p align=&quot;left&quot; style=&quot;margin-left:10px;font-family:verdana, arial, tahoma;font-size:14px;color:#333333;text-indent:2em;background-color:#FFFFFF;&quot;&gt;
	　这些预装软件中既有手机厂商自家的应用也有第三方厂商的软件，背后以及形成了利益链条。预装软件悄悄运行，消耗流量甚至窃取用户隐私给消费者造成很大困扰。&amp;nbsp;
&lt;/p&gt;
&lt;div align=&quot;left&quot; style=&quot;color:#333333;font-family:verdana, arial, tahoma;font-size:14px;background-color:#FFFFFF;&quot;&gt;
	　不过，业内人士对此并不乐观，坦言无法彻底解决手机预装软件乱象。
&lt;/div&gt;','1','1389690299','1389690299','0');
DROP TABLE IF EXISTS `cx_page` ;
CREATE TABLE `cx_page` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shortname` char(30) NOT NULL COMMENT '短连接名称',
  `title` char(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text COMMENT '内容',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  `issystem` tinyint(1) DEFAULT '0' COMMENT '系统集成(1:是,0否)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
insert into `cx_page`(`id`,`shortname`,`title`,`content`,`click`,`updatetime`,`createtime`,`uid`,`issystem`) values('1','companyinfo','公司简介','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:12px;&quot;&gt;西安创想网络&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size:12px;&quot;&gt;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size:12px;&quot;&gt;www.cxiangnet.cn --------专注于网站建设、网站优化、网站推广、电子商务综合营销以及办公OA系统开发为一体的专业网络服务商。 创想网络成立于2009年，至今已经成功为西安数百家企业提供网站建设、网站维护、seo网站整体优化、企业内部系统开发以及电子商务综合营销服务，并且为数千家企业免费提供了专业知识咨询和技术引导。2011年成为陕西省就业中心重点扶持企业，并且和一系列权威单位建立了长期合作关系和提供专业技术支持。在政府的关怀支持下，在创想团队的不断努力下，创想网络不断扩大服务范围和群体；展望未来，创想人一定会用饱满的热情和优秀的服务回报广大新老客户的厚爱！&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; 多年来我们以“优质的服务，低廉的价格”为广大用户服务，为顾客成功做贡献，坚持“以人为本”的原则，实现企业与客户、企业与员工的全面发展，优化公司产品结构，针对客户个性化的需求，为客户提供“量身定制”的服务，满足每一个客户的需求。&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 理念文化&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 企业哲学：创新则兴，守旧则衰。理论创新拓思路、技术创新争领先，产品创新赢市场，管理创新得效益。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 企业宗旨：优质的服务，低廉的价格，为顾客成功做贡献，为客户创造价值，为员工创造机会，为社会创造财富。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 经营目标：创优质品牌，成为领先的电子商务领域服务提供商。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 企业精神：开拓创新，求实奋进 ，努力拼搏。&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 企业口号：宁可光荣战死，也不体面放弃！创西安第一自主网络品牌！&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 核心价值观&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 人尽其才，以人为本——人才是企业发展的根本，合理利用人才是企业发展的保证。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 终身学习，追求进步——鄙弃小富即安，努力学习新知识，新技能，不断追求进步。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 产品服务，力争完美——所有的产品和服务虽然不可能完美，但要力求完美，保证成熟，可靠。&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 人才价值观&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 诚信、尊重——诚实守信，尊重他人，尊重企业。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 创新、合作——打破成规，力求突破，具备合作精神，服从企业发展。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:12px;&quot;&gt; 学习、发展——具有学习能力和学习精神，追求进步与发展。&lt;/span&gt; 
&lt;/p&gt;
&lt;h3 style=&quot;text-indent:2em;&quot;&gt;
	&lt;a href=&quot;http://www.oschina.net/p/freeglobes&quot; target=&quot;_blank&quot;&gt;&lt;/a&gt; 
&lt;/h3&gt;','1','1390554885','0','0','1');
insert into `cx_page`(`id`,`shortname`,`title`,`content`,`click`,`updatetime`,`createtime`,`uid`,`issystem`) values('2','contactus','联系我们','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	QQ:137898350
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	邮箱:137898350@qq.com
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;margin-left:40px;&quot;&gt;
	&lt;iframe src=&quot;http://www.cxiangnet.cn/view/admin/public/kindeditor/plugins/baidumap/index.html?center=108.953098%2C34.2778&amp;zoom=11&amp;width=558&amp;height=360&amp;markers=108.953098%2C34.2778&amp;markerStyles=l%2CA&quot; frameborder=&quot;0&quot; style=&quot;width:560px;height:362px;&quot;&gt;
	&lt;/iframe&gt;
&amp;nbsp;
&lt;/p&gt;
&lt;p&gt;
	&lt;br /&gt;
&lt;/p&gt;','1','1389528758','0','0','1');
insert into `cx_page`(`id`,`shortname`,`title`,`content`,`click`,`updatetime`,`createtime`,`uid`,`issystem`) values('3','aboutus','关于我们','&lt;p&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; php外包,网站开发,企业建站.........
&lt;/p&gt;
&lt;p&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; &lt;a href=&quot;http://simplecms.cxiangnet.cn&quot; target=&quot;_blank&quot;&gt;SimpleCMS&lt;/a&gt;企业建站系统
&lt;/p&gt;
&lt;p&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; &lt;a href=&quot;http://simplephp.cxiangnet.cn&quot; target=&quot;_blank&quot;&gt;SimplePHP&lt;/a&gt; php开发框架
&lt;/p&gt;
&lt;p&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; 相关案例:
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp; 整理中......
&lt;/p&gt;','1','1389528900','0','0','1');
insert into `cx_page`(`id`,`shortname`,`title`,`content`,`click`,`updatetime`,`createtime`,`uid`,`issystem`) values('4','culture','企业文化','&lt;p&gt;
	&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;
&lt;/p&gt;
&lt;p&gt;
	&amp;nbsp; &amp;nbsp; 自由软件开发 ...
&lt;/p&gt;','1','1389528919','1389273828','0','0');
DROP TABLE IF EXISTS `cx_product` ;
CREATE TABLE `cx_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(5) DEFAULT NULL COMMENT '产品分类',
  `name` varchar(50) DEFAULT '' COMMENT '产品名称',
  `mall` varchar(500) DEFAULT NULL COMMENT '购买链接',
  `keywords` varchar(50) DEFAULT '' COMMENT '产品关键词',
  `description` text COMMENT '产品描述',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '产品价格',
  `promotion` decimal(10,2) DEFAULT '0.00' COMMENT '特价',
  `brand` varchar(50) DEFAULT NULL COMMENT '品牌',
  `model` varchar(50) DEFAULT NULL COMMENT '型号',
  `color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `amount` int(5) DEFAULT '1' COMMENT '数量',
  `origin` varchar(30) DEFAULT '' COMMENT '产地',
  `unit` varchar(15) DEFAULT NULL COMMENT '单位',
  `pics` varchar(500) DEFAULT NULL COMMENT '上传图片id',
  `picindex` int(5) DEFAULT NULL COMMENT '首张图片',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('6','12','易王重庆大排档','http://www.ywcqdpd.com','易王,大排档','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;开发技术:html+css3+php+mysql&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span&gt;&lt;span style=&quot;line-height:27px;&quot;&gt;网站地址:www.ywcqdpd.com&lt;/span&gt;&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;中国易王重庆大排档公司成立于1996年。这10年间，做为重庆美食领域的航姆发展迅速，在国内成立几家分店。并迅速成为当地一个特色。不仅如此，易王重庆大排档以主要经营砂锅为特色在每个地方迅速落地生根。把砂锅推向了更高层次的境界。荣获了多次国家、省、市颁发的荣誉扁牌。易王重庆大排档的口碑已经为渭南市乃至到&lt;/span&gt;
&lt;/p&gt;','2000.00','1988.00','易王','企业站','红色','1','西安','套','19','19','1389927598','1389927598');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('7','12','马治权个人网站','http://www.mazhiquan.com','马治权,个人网站','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:thinkphp+html+css&lt;/span&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;a href=&quot;http://www.mazhiquan.com&quot; target=&quot;_blank&quot;&gt;www.mazhiquan.com&lt;/a&gt;&lt;/span&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;马治权，男。大学文化程度。著名作家、书法家。 1987年大学毕业后分配在陕西省政协工作。 1988年深入西安电影制片厂调查研究，为时一年零三个月，调查结束后写成10多万字的调查报告《涡漩中的西影》。 1992年创办《各界》杂志，历时十年，将《各界》推向全国，成为政协系统名刊，入“中国精品期刊”系列。 马治权有著作多部，已出版有杂文集、散文集和长篇小说。2003年杂文入选王蒙主编的《中国最佳杂文选》；2004年散文获《人民日报》、《新闻出版报》、《中国文化报》、《中国作家》杂志、《纵横》杂志等媒体征文大赛一等奖。 马治权6岁习练书法，曾临过王羲之、颜真卿、柳公权、苏轼、黄庭坚、米芾、傅山、何绍基、于右任等，并由此上溯魏碑、汉隶、秦篆。他的书法深厚清雄，静逸萧散，颇得行家好评。中国书协副主席、著名书法评论家钟明善，著名书法家茹桂，著名作家贾平凹、冯骥才、张子良、商子雍、池莉、韩小蕙对其书法均有文章评论。卫俊秀先...&lt;/span&gt; 
&lt;/p&gt;','2000.00','2000.00','马治权','个人网站','红黑','1','西安','套','20','20','1389927794','1389929815');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('8','8','SimpleCMS','http://simplephp.cxiangnet.cn','SimpleCMS,SimplePHP,企业建站','&lt;h1 style=&quot;text-indent:2em;&quot;&gt;
	网站技术:SimplePHP+html+js+css
&lt;/h1&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	网站地址:&lt;a href=&quot;http://simplecms.cxiangnet.cn&quot; target=&quot;_blank&quot;&gt;simplecms.cxiangnet.cn&lt;/a&gt;
&lt;/p&gt;
&lt;h1 style=&quot;text-indent:2em;&quot;&gt;
	欢迎使用 SimpleCMS ！
&lt;/h1&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	这是您第一次与SimpleCMS接触，SimpleCMS设计初衷就是为了让设计师、程序员、以及不懂网站的初学者能够轻松建立网站。
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	SimpleCMS还有许多您不知道的小秘密，希望我们在以后的日子里相处愉快。
&lt;/p&gt;','3500.00','2500.00','simplecms','simplecms v 1.2','黑白','230','西安','套','21,22','21','1389929786','1389929786');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('9','8','微信商城系统','http://wxshop.cxiangnet.cn','微信商城,微商城','&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;&lt;img src=&quot;/static/uploads/20140117/20140117165325_94813.png&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/static/uploads/20140117/20140117165412_12237.png&quot; alt=&quot;&quot; /&gt;&lt;br /&gt;
&lt;/span&gt; 
&lt;/p&gt;
&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;&lt;br /&gt;
&lt;/span&gt; 
&lt;/p&gt;
&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;PHP微信商城源码,MYSQL数据库,支持会员注册,会员支付（支付宝）,微信商城系统,内部集成微信公众平台接口,可同步微信平台信息,适合做微信营销系统,独家发布！！！&lt;/span&gt; 
&lt;/p&gt;','2000.00','1500.00','微信商城','wxshop','灰色','13456','西安','套','23,24','23','1389948828','1389948861');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('10','12','西安车小二','http://www.456789.cn','西安车小二,西安汽车违章,西安在线消违章','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:ThinkPHP+html+js&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.456789.cn&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.456789.cn&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;西安车小二 www.456789.cn 专业从事汽车后市场相关业务网站，诸如提供西安交通违章查询、西安汽车审验年检、西安车辆过户挂牌、西安驾照审验换证等汽车业务。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;首推西安车务网上办理新模式。西安汽车年审、西安车辆过户在线预约功能；在线办理西安驾照审验换证、车辆异地委托书业务；真正实现了足不出户办理车辆相关业务，为广大车主提供了省时、省力、省心的新方式。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;我们的目标是：为广大车主分忧解难，让广大车主享受快乐驾驶的车生活。欢迎您亲临公司参观洽谈或来电咨询。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;','6000.00','6000.00','汽车违章','汽车违章','白色','1','西安','套','25','25','1390044560','1390044560');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('11','12','迈博商贸公司','','迈博商贸','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	网站技术:SimplePHP+html+js+css
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.maibosm.com&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.maibosm.com&lt;/span&gt;&lt;/a&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;西安迈博商贸有限公司介绍：&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　西安迈博广告标牌成立于2013年。创立之初，公司就开创性地提出了空间导向识别系统的概念，以先进的设计理念、强大的专业设计队伍进入市场，依托世界先进水平的加工设备与制造工艺，秉承服务创造价值的信条，快速成长为中国标识加工制造业的皎皎者。公司的主要业务范围包括企业VI应用、公共空间导向标识、展览展示工程、楼体大字、路牌、灯箱、霓虹灯的设计、制造、安装。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 经营理念：&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　诚信：诚信是迈博人前行的基础，以诚为本是我们永远不变的核心；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　积极：积极是迈博人的态度，涵盖着我们的一切状态；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　专业：专业是迈博人的优势，专业完美是我们永恒不变的追求；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　满意：客户满意是迈博人最大的业绩表现，我们竭尽所能以实现客户最高满意度；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　共赢：共赢是迈博人最终的诉求，是我们长期坚持追求的辉煌结果。&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　主要服务项目：西安标识标牌，西安标识，西安标牌，西安标牌制作，西安门牌&lt;/span&gt; 
&lt;/p&gt;','2000.00','2000.00','','','','0','西安','','26,27','26','1390549817','1390551006');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('12','12','麦迪广告传媒公司','','麦迪广告,麦迪传媒','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:SimplePHP+html+js&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.midead.com/index.php&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.midead.com/index.php&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;&amp;nbsp;&amp;nbsp;西安麦迪广告传媒有限公司，是一家以户外广告传媒为主的专业化、综合性广告公司公司致力于户外媒体的开发、代理、整合、媒体监测为主的集约式运作模式的传媒公司，整合了西安本土媒体的资源，将精确、准确的媒体信息第一时间传达给广告主和各大代理公司，我们把优质服务、优势媒体视为企业的核心竞争力。&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; &amp;nbsp; &amp;nbsp;西安麦迪广告传媒有限公司致力于打造西安户外广告专业的服务平台，建立起一套全程无忧的管理系统，以保证客户广告的传播准确最大化，媒体优质化。向客户推出“媒体管家”的服务承诺”，打造户外优质服务系统，以非凡创意，精良制作，提供从媒体选点、论证、形式设计、媒体施工、工程监理、工程验收及售后监控的系统服务。&lt;/span&gt;
&lt;/p&gt;','2000.00','2000.00','','','','1','西安','','28,29','0','1390553327','1390553327');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('13','12','陕北白绒山羊研究所','http://www.chinagoats.com/','陕北白绒山羊,山羊,研究所','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:ASP+html+js+css&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.chinagoats.com/&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.chinagoats.com&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-family:Simsun;font-size:medium;line-height:normal;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;/span&gt;&lt;br /&gt;
&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-family:Simsun;font-size:18px;line-height:normal;&quot;&gt;陕西省陕北绒山羊工程技术研究中心&lt;/span&gt;
&lt;/p&gt;','3000.00','3000.00','','','','0','西安','','30,31','30','1390553856','1390553856');
insert into `cx_product`(`id`,`cateid`,`name`,`mall`,`keywords`,`description`,`price`,`promotion`,`brand`,`model`,`color`,`amount`,`origin`,`unit`,`pics`,`picindex`,`createtime`,`updatetime`) values('5','12','微信淘宝客 好聚美','http://www.haojumei.com','微信,淘宝客,好聚美','&lt;p&gt;
	1.微信淘宝客系统
&lt;/p&gt;
&lt;p&gt;
	2.完美的界面
&lt;/p&gt;
&lt;p&gt;
	3.html5+css3
&lt;/p&gt;','2000.00','2000.00','好聚美 ','微信','蓝色','123','西安','套','18','18','1389577175','1389577175');
DROP TABLE IF EXISTS `cx_setting` ;
CREATE TABLE `cx_setting` (
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '名称',
  `key` varchar(30) NOT NULL DEFAULT '' COMMENT '键',
  `value` text NOT NULL COMMENT '值',
  `inputtype` enum('input','radio','checkbox','select','textarea') DEFAULT 'input' COMMENT '输入框类型',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站配置';
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('时区','timezone','+8','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('域名','domain','http://www.cxiangnet.cn','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('邮箱','mail','137898350@qq.com','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('版权','siteright','创想网路工作室','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('ICP备案号','icp',' 陕ICP备09000692号','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('网站关键词','keywords','西安企业建站,php外包,SimpleCMS,SimplePHP,企业建站','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('网站描述','description','SimpleCMS,软件外包,php开发','textarea','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('主题模板','theme','cxnet','select','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('网站名称','sitename','创想网络工作室','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('后台日志','admin_log','0','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('公司地址','address','西安市','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('公司电话','phone','02986256570','input','0');
insert into `cx_setting`(`name`,`key`,`value`,`inputtype`,`type`) values('统计代码','cnzz','<script src=\"http://s13.cnzz.com/stat.php?id=1248466&web_id=1248466\" language=\"JavaScript\"></script>','textarea','0');
DROP TABLE IF EXISTS `cx_show` ;
CREATE TABLE `cx_show` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cateid` int(11) NOT NULL DEFAULT '0' COMMENT '分类',
  `title` varchar(30) DEFAULT '' COMMENT '标题',
  `content` text NOT NULL COMMENT '内容',
  `pics` varchar(250) NOT NULL DEFAULT '' COMMENT '图片',
  `picindex` int(11) DEFAULT '0' COMMENT '首张图片',
  `createtime` int(11) DEFAULT '0' COMMENT '创建时间',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('6','12','易王重庆大排档','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;开发技术:html+css3+php+mysql&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span&gt;&lt;span style=&quot;line-height:27px;&quot;&gt;网站地址:www.ywcqdpd.com&lt;/span&gt;&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;中国易王重庆大排档公司成立于1996年。这10年间，做为重庆美食领域的航姆发展迅速，在国内成立几家分店。并迅速成为当地一个特色。不仅如此，易王重庆大排档以主要经营砂锅为特色在每个地方迅速落地生根。把砂锅推向了更高层次的境界。荣获了多次国家、省、市颁发的荣誉扁牌。易王重庆大排档的口碑已经为渭南市乃至到&lt;/span&gt;
&lt;/p&gt;','19','0','1389927598','1389927598');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('7','12','马治权个人网站','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:thinkphp+html+css&lt;/span&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;a href=&quot;http://www.mazhiquan.com&quot; target=&quot;_blank&quot;&gt;www.mazhiquan.com&lt;/a&gt;&lt;/span&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;马治权，男。大学文化程度。著名作家、书法家。 1987年大学毕业后分配在陕西省政协工作。 1988年深入西安电影制片厂调查研究，为时一年零三个月，调查结束后写成10多万字的调查报告《涡漩中的西影》。 1992年创办《各界》杂志，历时十年，将《各界》推向全国，成为政协系统名刊，入“中国精品期刊”系列。 马治权有著作多部，已出版有杂文集、散文集和长篇小说。2003年杂文入选王蒙主编的《中国最佳杂文选》；2004年散文获《人民日报》、《新闻出版报》、《中国文化报》、《中国作家》杂志、《纵横》杂志等媒体征文大赛一等奖。 马治权6岁习练书法，曾临过王羲之、颜真卿、柳公权、苏轼、黄庭坚、米芾、傅山、何绍基、于右任等，并由此上溯魏碑、汉隶、秦篆。他的书法深厚清雄，静逸萧散，颇得行家好评。中国书协副主席、著名书法评论家钟明善，著名书法家茹桂，著名作家贾平凹、冯骥才、张子良、商子雍、池莉、韩小蕙对其书法均有文章评论。卫俊秀先...&lt;/span&gt; 
&lt;/p&gt;','20','0','1389927794','1389929815');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('8','8','SimpleCMS','&lt;h1 style=&quot;text-indent:2em;&quot;&gt;
	网站技术:SimplePHP+html+js+css
&lt;/h1&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	网站地址:&lt;a href=&quot;http://simplecms.cxiangnet.cn&quot; target=&quot;_blank&quot;&gt;simplecms.cxiangnet.cn&lt;/a&gt;
&lt;/p&gt;
&lt;h1 style=&quot;text-indent:2em;&quot;&gt;
	欢迎使用 SimpleCMS ！
&lt;/h1&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	这是您第一次与SimpleCMS接触，SimpleCMS设计初衷就是为了让设计师、程序员、以及不懂网站的初学者能够轻松建立网站。
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	SimpleCMS还有许多您不知道的小秘密，希望我们在以后的日子里相处愉快。
&lt;/p&gt;','21,22','0','1389929786','1389929786');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('9','8','微信商城系统','&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;&lt;img src=&quot;/static/uploads/20140117/20140117165325_94813.png&quot; alt=&quot;&quot; /&gt;&lt;img src=&quot;/static/uploads/20140117/20140117165412_12237.png&quot; alt=&quot;&quot; /&gt;&lt;br /&gt;
&lt;/span&gt; 
&lt;/p&gt;
&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;&lt;br /&gt;
&lt;/span&gt; 
&lt;/p&gt;
&lt;p&gt;
	&lt;span style=&quot;color:#222222;font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:21px;background-color:#FFFFFF;&quot;&gt;PHP微信商城源码,MYSQL数据库,支持会员注册,会员支付（支付宝）,微信商城系统,内部集成微信公众平台接口,可同步微信平台信息,适合做微信营销系统,独家发布！！！&lt;/span&gt; 
&lt;/p&gt;','23,24','0','1389948828','1389948861');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('10','12','西安车小二','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:ThinkPHP+html+js&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.456789.cn&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.456789.cn&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;西安车小二 www.456789.cn 专业从事汽车后市场相关业务网站，诸如提供西安交通违章查询、西安汽车审验年检、西安车辆过户挂牌、西安驾照审验换证等汽车业务。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;首推西安车务网上办理新模式。西安汽车年审、西安车辆过户在线预约功能；在线办理西安驾照审验换证、车辆异地委托书业务；真正实现了足不出户办理车辆相关业务，为广大车主提供了省时、省力、省心的新方式。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;我们的目标是：为广大车主分忧解难，让广大车主享受快乐驾驶的车生活。欢迎您亲临公司参观洽谈或来电咨询。&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;','25','0','1390044560','1390044560');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('11','12','迈博商贸公司','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	网站技术:SimplePHP+html+js+css
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.maibosm.com&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.maibosm.com&lt;/span&gt;&lt;/a&gt; 
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;西安迈博商贸有限公司介绍：&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　西安迈博广告标牌成立于2013年。创立之初，公司就开创性地提出了空间导向识别系统的概念，以先进的设计理念、强大的专业设计队伍进入市场，依托世界先进水平的加工设备与制造工艺，秉承服务创造价值的信条，快速成长为中国标识加工制造业的皎皎者。公司的主要业务范围包括企业VI应用、公共空间导向标识、展览展示工程、楼体大字、路牌、灯箱、霓虹灯的设计、制造、安装。&amp;nbsp;&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 经营理念：&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　诚信：诚信是迈博人前行的基础，以诚为本是我们永远不变的核心；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　积极：积极是迈博人的态度，涵盖着我们的一切状态；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　专业：专业是迈博人的优势，专业完美是我们永恒不变的追求；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　满意：客户满意是迈博人最大的业绩表现，我们竭尽所能以实现客户最高满意度；&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　共赢：共赢是迈博人最终的诉求，是我们长期坚持追求的辉煌结果。&lt;/span&gt;&lt;br /&gt;
&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; 　　主要服务项目：西安标识标牌，西安标识，西安标牌，西安标牌制作，西安门牌&lt;/span&gt; 
&lt;/p&gt;','26,27','0','1390549817','1390551006');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('12','12','麦迪广告传媒公司','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:SimplePHP+html+js&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.midead.com/index.php&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.midead.com/index.php&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;br /&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;&amp;nbsp;&amp;nbsp;西安麦迪广告传媒有限公司，是一家以户外广告传媒为主的专业化、综合性广告公司公司致力于户外媒体的开发、代理、整合、媒体监测为主的集约式运作模式的传媒公司，整合了西安本土媒体的资源，将精确、准确的媒体信息第一时间传达给广告主和各大代理公司，我们把优质服务、优势媒体视为企业的核心竞争力。&lt;/span&gt;&lt;br /&gt;
&lt;span style=&quot;font-size:18px;&quot;&gt; &amp;nbsp; &amp;nbsp;西安麦迪广告传媒有限公司致力于打造西安户外广告专业的服务平台，建立起一套全程无忧的管理系统，以保证客户广告的传播准确最大化，媒体优质化。向客户推出“媒体管家”的服务承诺”，打造户外优质服务系统，以非凡创意，精良制作，提供从媒体选点、论证、形式设计、媒体施工、工程监理、工程验收及售后监控的系统服务。&lt;/span&gt;
&lt;/p&gt;','28,29','0','1390553327','1390553327');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('13','12','陕北白绒山羊研究所','&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站技术:ASP+html+js+css&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-size:18px;&quot;&gt;网站地址:&lt;/span&gt;&lt;a href=&quot;http://www.chinagoats.com/&quot; target=&quot;_blank&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;www.chinagoats.com&lt;/span&gt;&lt;/a&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-family:Simsun;font-size:medium;line-height:normal;&quot;&gt;&lt;span style=&quot;font-size:18px;&quot;&gt;&lt;/span&gt;&lt;br /&gt;
&lt;/span&gt;
&lt;/p&gt;
&lt;p style=&quot;text-indent:2em;&quot;&gt;
	&lt;span style=&quot;font-family:Simsun;font-size:18px;line-height:normal;&quot;&gt;陕西省陕北绒山羊工程技术研究中心&lt;/span&gt;
&lt;/p&gt;','30,31','0','1390553856','1390553856');
insert into `cx_show`(`id`,`cateid`,`title`,`content`,`pics`,`picindex`,`createtime`,`updatetime`) values('5','12','微信淘宝客 好聚美','&lt;p&gt;
	1.微信淘宝客系统
&lt;/p&gt;
&lt;p&gt;
	2.完美的界面
&lt;/p&gt;
&lt;p&gt;
	3.html5+css3
&lt;/p&gt;','18','0','1389577175','1389577175');
