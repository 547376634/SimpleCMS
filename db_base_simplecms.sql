/*
SQLyog Ultimate v9.20 
MySQL - 5.5.27 : Database - db_cxnet
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_cxnet` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_cxnet`;

/*Table structure for table `simple_admin` */

DROP TABLE IF EXISTS `simple_admin`;

CREATE TABLE `simple_admin` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `username` varchar(50) NOT NULL COMMENT '管理员名称',
  `password` varchar(32) NOT NULL COMMENT '管理员密码',
  `roleid` int(1) unsigned NOT NULL COMMENT '管理员角色',
  `ipaddress` varchar(30) DEFAULT NULL COMMENT '登录IP',
  `lastlogintime` int(10) unsigned DEFAULT NULL COMMENT '最后登录时间',
  `logincount` int(6) unsigned NOT NULL DEFAULT '0' COMMENT '登录次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员表';

/*Data for the table `simple_admin` */

insert  into `simple_admin`(`id`,`username`,`password`,`roleid`,`ipaddress`,`lastlogintime`,`logincount`) values (1,'admin','21232f297a57a5a743894a0e4a801fc3',1,'127.0.0.1',1378361082,2);

/*Table structure for table `simple_category` */

DROP TABLE IF EXISTS `simple_category`;

CREATE TABLE `simple_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `parentid` int(11) DEFAULT '0' COMMENT '父类id',
  `categoryname` varchar(30) DEFAULT NULL COMMENT '分类名称',
  `type` enum('news','product','link','job') DEFAULT NULL COMMENT '类型',
  `issystem` tinyint(1) DEFAULT '0' COMMENT '系统集成(1:是,0:否)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `simple_category` */

insert  into `simple_category`(`id`,`parentid`,`categoryname`,`type`,`issystem`) values (1,0,'新闻','news',1),(2,1,'行业动态','news',1),(3,1,'企业新闻','news',1),(4,0,'招聘','job',1),(5,4,'社会招聘','job',1),(6,4,'校园招聘','job',1),(7,0,'产品','product',1),(8,7,'公司产品','product',1),(9,0,'链接','link',1),(10,9,'首页链接','link',1),(11,9,'内页链接','link',1);

/*Table structure for table `simple_file` */

DROP TABLE IF EXISTS `simple_file`;

CREATE TABLE `simple_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `filename` char(100) NOT NULL COMMENT '文件名称',
  `type` char(10) DEFAULT NULL COMMENT '文件类型',
  `size` int(11) DEFAULT NULL COMMENT '文件大小',
  `folder` char(25) DEFAULT NULL COMMENT '文件路径',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `simple_file` */

/*Table structure for table `simple_guestbook` */

DROP TABLE IF EXISTS `simple_guestbook`;

CREATE TABLE `simple_guestbook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(30) DEFAULT NULL COMMENT '用户名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型(0:主贴,1:回复)',
  `parentid` int(11) DEFAULT NULL COMMENT '主贴id',
  `content` text COMMENT '内容',
  `ipaddress` varchar(30) DEFAULT NULL COMMENT 'ip地址',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/*Data for the table `simple_guestbook` */

insert  into `simple_guestbook`(`id`,`username`,`email`,`type`,`parentid`,`content`,`ipaddress`,`createtime`,`updatetime`) values (1,'sunny','137898350@qq.com',0,NULL,'在阿迪vv阿斯顿v阿斯顿vas发','127.0.0.1',1378361082,1378361082),(2,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',1378361082,1378361082),(3,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(4,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(5,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(6,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(7,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(8,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(9,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(11,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(12,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(13,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(14,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(15,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(16,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(17,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(18,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(19,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(20,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(22,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(23,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(25,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(26,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(27,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(28,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(29,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(30,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(31,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(47,'admin',NULL,1,1,'阿斯顿v',NULL,1387268723,1387268723),(48,'admin',NULL,1,1,'阿斯顿v324132412','127.0.0.1',1387269018,1387269018),(33,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(34,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(35,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(36,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(37,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(38,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(39,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(40,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(41,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(42,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(43,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(44,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(45,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL),(46,'sunny5156','547376634@qq.com',0,NULL,'你么弄阿訇潘红军奥鹏就阿斯兰的窘迫阿斯顿 ','127.0.0.1',NULL,NULL);

/*Table structure for table `simple_job` */

DROP TABLE IF EXISTS `simple_job`;

CREATE TABLE `simple_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) DEFAULT '' COMMENT '招聘名称',
  `cateid` int(3) DEFAULT NULL COMMENT '招聘类型(1:社会招聘,2:校园招聘)',
  `nums` int(5) NOT NULL DEFAULT '1' COMMENT '招聘人数',
  `jobdes` text COMMENT '岗位描述',
  `email` varchar(50) DEFAULT NULL COMMENT '联系邮箱',
  `tel` varchar(50) DEFAULT NULL COMMENT '联系电话',
  `phone` varchar(50) DEFAULT NULL COMMENT '联系手机',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `simple_job` */

/*Table structure for table `simple_link` */

DROP TABLE IF EXISTS `simple_link`;

CREATE TABLE `simple_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(11) DEFAULT NULL COMMENT '分类id',
  `linkname` varchar(50) DEFAULT NULL COMMENT '连接名称',
  `type` tinyint(1) DEFAULT '1' COMMENT '类型1:文字2:图片',
  `linkurl` varchar(150) DEFAULT NULL COMMENT '连接地址',
  `logourl` varchar(200) DEFAULT NULL COMMENT 'logo地址',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `uid` int(11) DEFAULT NULL COMMENT '管理员ID',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `simple_link` */

/*Table structure for table `simple_mod` */

DROP TABLE IF EXISTS `simple_mod`;

CREATE TABLE `simple_mod` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) DEFAULT '' COMMENT '模块名称',
  `mod` varchar(20) NOT NULL DEFAULT '' COMMENT '模块',
  `issystem` tinyint(1) DEFAULT '1' COMMENT '是否系统模块',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `simple_mod` */

insert  into `simple_mod`(`id`,`name`,`mod`,`issystem`,`createtime`,`updatetime`) values (1,'新闻','news',1,1387421040,1387421040),(2,'产品','product',1,1387421040,1387421040),(3,'留言','guestbook',1,1387421040,1387421040),(4,'招聘','job',1,1387421040,1387421040),(5,'友链','link',1,1387421040,1387421040);

/*Table structure for table `simple_nav` */

DROP TABLE IF EXISTS `simple_nav`;

CREATE TABLE `simple_nav` (
  `name` varchar(30) NOT NULL COMMENT '名称',
  `key` varchar(30) NOT NULL COMMENT '键(header:头部导航,footer:底部导航)',
  `value` text COMMENT '值',
  `updatetime` int(11) DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `simple_nav` */

insert  into `simple_nav`(`name`,`key`,`value`,`updatetime`) values ('头部导航','header','a:2:{i:0;a:3:{s:4:\"type\";s:6:\"system\";s:2:\"id\";s:1:\"0\";s:4:\"name\";s:6:\"首页\";}i:1;a:3:{s:4:\"type\";s:7:\"product\";s:2:\"id\";s:1:\"8\";s:4:\"name\";s:12:\"公司产品\";}}',1388124734);

/*Table structure for table `simple_news` */

DROP TABLE IF EXISTS `simple_news`;

CREATE TABLE `simple_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(11) NOT NULL COMMENT '分类id',
  `title` varchar(30) NOT NULL COMMENT '标题',
  `keywords` varchar(150) DEFAULT NULL COMMENT '关键词',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `content` text COMMENT '内容',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `simple_news` */

/*Table structure for table `simple_page` */

DROP TABLE IF EXISTS `simple_page`;

CREATE TABLE `simple_page` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `shortname` char(30) NOT NULL COMMENT '短连接名称',
  `title` char(50) NOT NULL DEFAULT '' COMMENT '标题',
  `content` text COMMENT '内容',
  `click` int(11) DEFAULT '1' COMMENT '点击数',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `uid` int(11) DEFAULT NULL COMMENT '管理员id',
  `issystem` tinyint(1) DEFAULT '0' COMMENT '系统集成(1:是,0否)',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `simple_page` */

insert  into `simple_page`(`id`,`shortname`,`title`,`content`,`click`,`updatetime`,`createtime`,`uid`,`issystem`) values (1,'companyinfo','公司简介','公司简介',1,NULL,NULL,NULL,1),(2,'contactus','联系我们','联系我们',1,1388123348,NULL,NULL,1),(3,'aboutus','关于我们','aboutus',1,NULL,NULL,NULL,1);

/*Table structure for table `simple_product` */

DROP TABLE IF EXISTS `simple_product`;

CREATE TABLE `simple_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cateid` int(5) DEFAULT NULL COMMENT '产品分类',
  `name` varchar(50) DEFAULT '' COMMENT '产品名称',
  `mall` varchar(500) DEFAULT NULL COMMENT '购买链接',
  `keywords` varchar(50) DEFAULT '' COMMENT '产品关键词',
  `description` text COMMENT '产品描述',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '产品价格',
  `promotion` decimal(10,2) DEFAULT '0.00' COMMENT '特价',
  `brand` varchar(50) DEFAULT NULL COMMENT '品牌',
  `model` varchar(50) DEFAULT NULL COMMENT '型号',
  `color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `amount` int(5) DEFAULT '1' COMMENT '数量',
  `origin` varchar(30) DEFAULT '' COMMENT '产地',
  `unit` varchar(15) DEFAULT NULL COMMENT '单位',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `simple_product` */

/*Table structure for table `simple_setting` */

DROP TABLE IF EXISTS `simple_setting`;

CREATE TABLE `simple_setting` (
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '名称',
  `key` varchar(30) NOT NULL DEFAULT '' COMMENT '键',
  `value` varchar(250) NOT NULL DEFAULT '' COMMENT '值',
  `inputtype` enum('input','radio','checkbox','select','textarea') DEFAULT 'input' COMMENT '输入框类型',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='网站配置';

/*Data for the table `simple_setting` */

insert  into `simple_setting`(`name`,`key`,`value`,`inputtype`,`type`) values ('时区','timezone','+8','input',0),('域名','domain','http://simplecms.cxiangnet.cn','input',0),('邮箱','mail','admin@admin.com','input',0),('版权','siteright','SimpleCMS','input',0),('ICP备案号','icp','陕ICP11111111','input',0),('网站关键词','keywords','SimpleCMS,SimplePHP','input',0),('网站描述','description','Aimee','textarea',0),('主题模板','theme','ad860','select',0),('网站名称','sitename','SimpleCMS','input',0),('后台日志','admin_log','0','input',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
