<?php
/**
 * 新闻控制器
 * @author sunny5156 <blog.cxiangnet.cn>
 *
 */
class newsController extends Controller{
	
	private $leftNav = array();
	
	public function __construct(){
		parent::__construct();
		
		//左侧导航
		$this->leftNav = M('category')->where("type = 'news' AND parentid != 0")->order('id ASC')->limit(0, 5)->getAll();
		$this->assign('leftNav', $this->leftNav);
	}
	public function index(){
		
		$cid = intval($_GET['cid']);
		$cate = M('category')->where("type = 'news' AND parentid != 0")->order('id ASC')->getAll();
		foreach($cate as $v){
			$data[$v['id']]['categoryname'] = $v['categoryname'];
			$data[$v['id']]['list'] = M('news')->field('id,cateid,title,description,click,createtime')->where('cateid = '.$v['id'])->order('id DESC')->limit(0, 10)->getAll();
		}
		$this->assign('data', $data);
		$this->display('index.html');
	}
	public function clist(){
		
		$cid = intval($_GET['cid']);
		$page = empty($_GET['page']) ?1:intval($_GET['page']);
		$cate = M('category')->where('id='.$cid)->getOne();
		$data = M('news')->where('cateid = '.$cid)->order('createtime DESC')->page($page)->getAll();
// 		debug($data,0);
		$pager = M('news')->getPager($page, 'default/news/clist',array('cid'=>$cid));
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('pager', $pager);
		$this->display('clist.html');
	}
	
	public function detail(){
		$id = intval($_GET['id']);
		$data = M('news')->where('id='.$id)->getOne();
		$data['content'] = html_decode($data['content']);
		
		//上下一条新闻
		$data['prev'] =  M('news')->field('id,title')->order('id>='.$id.',id desc')->limit(0, 1)->getOne();
		$data['next'] =  M('news')->field('id,title')->order('id<='.$id.',id asc')->limit(0, 1)->getOne();
		
// 		debug($data);
		
		//相关新闻
		
		$this->assign('data', $data);
		$this->display('detail.html');
	}
	
}