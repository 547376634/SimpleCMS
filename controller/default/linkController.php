<?php
/**
 * 内链控制器
 * @author sunny5156 <blog.cxiangnet.cn>
 *
 */
class linkController extends publicController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$data = array();
		//图片链接 2
		$data[2] = M('link')->where('type = 2')->order('id DESC')->limit(0, 20)->getAll();
		
		//文字连接 1
		$data[1] = M('link')->where('type = 1')->order('id DESC')->limit(0, 20)->getAll();
		
		$this->assign('data', $data);
		$this->display('index.html');
	}
	
	public function clist(){
		
	}
	
	public function jump(){
		//@todo 链接跳转 统计信息
	}
}