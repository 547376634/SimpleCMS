<?php


class productController extends publicController{
	
	private $leftNav = array();
	
	public function __construct(){
		parent::__construct();
		
		//左侧导航
		$this->leftNav = M('category')->where("type = 'product' AND parentid != 0")->order('id ASC')->limit(0, 5)->getAll();
		$this->assign('leftNav', $this->leftNav);
	}
	/**
	 * 模块首页页面
	 */
	public function index(){
		$cid = intval($_GET['cid']);
		$cate = M('category')->where("type = 'product' AND parentid != 0")->order('id ASC')->getAll();
		foreach($cate as $v){
			$data[$v['id']]['categoryname'] = $v['categoryname'];
			$data[$v['id']]['list'] = M('product')->field('id,cateid,name,keywords AS description,createtime')->where('cateid = '.$v['id'])->order('id DESC')->limit(0, 10)->getAll();
		}
		$this->assign('data', $data);
		$this->display('index.html');
	}
	/**
	 * 分类列表 cate list
	 */
	public function clist(){
		$cid = intval($_GET['cid']);
		$page = empty($_GET['page'])?1:intval($_GET['page']);
		$cate = M('category')->where('id='.$cid)->getOne();
		$data = M('product')->where('cateid = '.$cid)->order('id DESC')->page($page)->getAll();
		foreach($data as &$v){
			$v['pics'] = D('file')->getPicList($v['pics']);
		}
// 		debug($data,0);
		$pager = M('product')->getPager($page, 'default/product/clist',array('cid'=>$cid));
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('pager', $pager);
		
		$this->display('clist.html');
	}
	/**
	 * 详细页面
	 */
	public function detail(){
		$id = intval($_GET['id']);
		$data = M('product')->where('id='.$id)->getOne();
		//封面
		if($data['picindex'] == 0 ){
			$tmp = array();
			$tmp = explode(',', $data['pics']);
			$data['picindex'] = $tmp[0];
			unset($tmp);
		}
		$data['description'] = html_decode($data['description']);
		$cate = M('category')->where('id='.$data['cateid'])->getOne();
		$data['pics'] = D('file')->getPicList($data['pics']);
		
		//上下一条新闻
		$data['prev'] =  M('product')->field('id,name')->order('id>='.$id.',id desc')->limit(0, 1)->getOne();
		$data['next'] =  M('product')->field('id,name')->order('id<='.$id.',id asc')->limit(0, 1)->getOne();
		
// 		debug($data,0);
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->display('detail.html');
	}
}