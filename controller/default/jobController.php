<?php
/**
 * 招聘控制器
 * @author sunny5156 <blog.cxiangnet.cn>
 *
 */
class jobController extends Controller{
	
	private $leftNav = array();
	
	public function __construct(){
		parent::__construct();
		
		//左侧导航
		$this->leftNav = M('category')->where("type = 'job' AND parentid != 0")->order('id ASC')->limit(0, 5)->getAll();
		$this->assign('leftNav', $this->leftNav);
	}
	/**
	 * 模块首页页面
	 */
	public function index(){
		
		$cid = intval($_GET['cid']);
		$cate = M('category')->where("type = 'job' AND parentid != 0")->order('id ASC')->getAll();
		foreach($cate as $v){
			$data[$v['id']]['categoryname'] = $v['categoryname'];
			$data[$v['id']]['list'] = M('job')->field('id,cateid,name,nums,createtime')->where('cateid = '.$v['id'])->order('id DESC')->limit(0, 10)->getAll();
		}
		$this->assign('data', $data);
		$this->display('index.html');
	}
	/**
	 * 分类列表 cate list
	 */
	public function clist(){

		$cid = intval($_GET['cid']);
		$page = empty($_GET['page']) ?1:intval($_GET['page']);
		$cate = M('category')->where('id='.$cid)->getOne();
		$data = M('job')->where('cateid = '.$cid)->order('id DESC')->page($page)->getAll();
		// 		debug($data,0);
		$pager = M('job')->getPager($page,'default/job/clist',array('cid'=>$cid));
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('pager', $pager);
		$this->display('clist.html');
		
	}
	/**
	 * 详细页面
	 */
	public function detail(){
		
		$id = intval($_GET['id']);
		$data = M('job')->where('id='.$id)->getOne();
		$cate = M('category')->where('id='.$data['cateid'])->getOne();
		$data['jobdes'] = html_decode($data['jobdes']);
		
		//上下一条新闻
		$data['prev'] =  M('job')->field('id,name')->order('id>='.$id.',id desc')->limit(0, 1)->getOne();
		$data['next'] =  M('job')->field('id,name')->order('id<='.$id.',id asc')->limit(0, 1)->getOne();
		
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->display('detail.html');
	}
}