<?php
/**
 * 单页控制器
 * @author sunny5156 <blog.cxiangnet.cn>
 *
 */
class pageController extends publicController{
	
	private $leftNav = array();
	
	public function __construct(){
		parent::__construct();
		//左侧导航
		$this->leftNav = M('page')->order('id ASC')->limit(0, 4)->getAll();
		$this->assign('leftNav', $this->leftNav);
	}
	
	public function detail(){
		
		$where = '';
		if(isset($_GET['name'])){
			$shortname = trim($_GET['name']);
			$where = "shortname ='{$shortname}'";
		}
		if(isset($_GET['id'])) {
			$id = intval($_GET['id']);
			$where = "id={$id}";
		}
		$data = M('page')->where($where)->getOne();
		$data['content'] = html_decode($data['content']);
		$this->assign('data', $data);
		$this->display('detail.html');
	}
}