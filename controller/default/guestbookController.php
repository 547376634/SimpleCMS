<?php
/**
 *留言板控制器
 * @author sunny5156 <blog.cxiangnet.cn>
 *
 */
class guestbookController extends publicController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		
		$page = empty($_GET['page']) ?1:intval($_GET['page']);
		$data = M('guestbook')->where('type = 0')->order('id DESC')->page($page)->getAll();
// 		debug($data,0);
		$pager = M('guestbook')->getPager($page, 'default/guestbook/index');
		
		$this->assign('data', $data);
		$this->assign('pager', $pager);
		
		$this->display('index.html');
	}
	
	public function insert(){
		if(isPost()){
			$data = array();
			$data['username'] = html_encode($_POST['username']);
			$data['phone'] = $_POST['phone'];
			$data['email'] = $_POST['email'];
			$data['content'] = html_encode($_POST['content']);
			$data['ipaddress'] = getClientIp();
			$data['createtime'] = $data['updatetime'] = time();
			
			$id= M('guestbook')->insert($data);
			redirect(U('default/guestbook/index'));
		}
		
	}
	
}