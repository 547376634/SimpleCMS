<?php


class showController extends publicController{
	
	private $leftNav = array();
	
	public function __construct(){
		parent::__construct();
		
		//左侧导航
		//$this->leftNav = M('category')->where("type = 'show' AND parentid != 0")->order('id ASC')->limit(0, 5)->getAll();
		//$this->assign('leftNav', $this->leftNav);
	}
	/**
	 * 模块首页页面
	 */
	public function index(){
		$cid = intval($_GET['cid']);
		$cate = M('category')->where("type = 'show' AND parentid != 0")->order('id ASC')->getAll();
		foreach($cate as $v){
			$data[$v['id']]['categoryname'] = $v['categoryname'];
			$data[$v['id']]['list'] = M('product')->field('id,cateid,name,keywords AS description,createtime')->where('cateid = '.$v['id'])->order('id DESC')->limit(0, 10)->getAll();
		}
		$this->assign('data', $data);
		$this->display('index.html');
	}
	/**
	 * 分类列表 cate list
	 */
	public function clist(){
		$cid = intval($_GET['cid']);
		//类型
		$type = $_GET['type'];
		
		$page = empty($_GET['page'])?1:intval($_GET['page']);
		$cate = M('category')->where('id='.$cid)->order('id DESC')->getOne();
		$tmp = explode('-', $cate['categoryname']);
		if (!empty($tmp)) {
			$cate['categoryname'] = $tmp[0];
			$cate['categoryenname'] = $tmp[1];
		}
		
		
		$data = M('show')->where('cateid = '.$cid)->order('id DESC')->page($page)->getAll();
// 		debug($data,0);
		foreach($data as &$v){
			if ($v['pics']) {
				$v['pics'] = D('file')->getPicList($v['pics']);
				sort($v['pics']);
			}
			$v['content'] = html_decode($v['content']);
			
		}
		
		$pager = M('show')->getPager($page, 'default/show/clist',array('cid'=>$cid));
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('pager', $pager);
		
		//左侧导航
		$leftNav = M('category')->where(" parentid = {$cate['parentid']}")->order('id ASC')->getAll();
		foreach ($leftNav as &$v){
			$tmp = array();
			$tmp = explode('-', $v['categoryname']);
			if (!empty($tmp)) {
				$v['categoryname'] = $tmp[0];
			}
		}
		$this->assign('leftNav', $leftNav);
		$this->assign('cid', $cid);
		
// 		debug($leftNav);
		
		if($type == 'dm'){
			$this->display('dm_clist.html');
		}else{
			$this->display('clist.html');
		}
	}
	/**
	 * 详细页面
	 */
	public function detail(){
		$id = intval($_GET['id']);
		$data = M('show')->where('id='.$id)->getOne();
		
		//类型
		$type = $_GET['type'];
		
		//封面
		if($data['picindex'] == 0 ){
			$tmp = array();
			$tmp = explode(',', $data['pics']);
			$data['picindex'] = $tmp[0];
			unset($tmp);
		}
		$data['description'] = html_decode($data['description']);
		$data['content'] = html_decode($data['content']);
		$cate = M('category')->where('id='.$data['cateid'])->getOne();
		if($data['pics'])$data['pics'] = D('file')->getPicList($data['pics']);
		
		//上下一条新闻
		$data['prev'] =  M('show')->field('id,title')->order('id>='.$id.',id desc')->limit(0, 1)->getOne();
		$data['next'] =  M('show')->field('id,title')->order('id<='.$id.',id asc')->limit(0, 1)->getOne();
		
// 		debug($data,0);
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		if($type == 'dm'){
			$this->display('dm_detail.html');
		}else{
			$this->display('detail.html');
		}
	}
}