<?php
class guestbookController extends commonController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function guestbookManage(){
		
		$data = array();
		$page = empty($_GET['page'])?1:$_GET['page'];
		
		$data = M('guestbook')->page($page)->where('type = 0')->getAll();//主贴
		
		$page = M('guestbook')->getPager($page, 'admin/guestbook/guestbookManage');
		
		$this->assign('page', $page);
		$this->assign('data', $data);
		$this->display('guestbookmanage.html');
	}
	
	public function getGuestbook(){
		$id = $_GET['id'];
		$data = M('guestbook')->where('id = '.$id)->getOne();
		
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'查询失败')));
		}
	}
	
	public function reply(){
		$id = intval($_GET['id']);
		if(isPost()){
			$data = $_POST;
			$data['type'] = 1;//回复
			$data['createtime'] = $data['updatetime'] = time();
			$data['ipaddress'] = getClientIp();
			M('guestbook')->insert($data);
			
		}else{
			$data = M('guestbook')->where('id = '.$id)->getOne();
			
			//获取回复
			$data['replys'] = M('guestbook')->where('parentid = '.$id.' AND type =1')->getAll();
			
			$this->assign('data', $data);
			$this->display('reply.html');
		}
		
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('guestbook')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
}