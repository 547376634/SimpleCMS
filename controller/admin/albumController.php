<?php
	/**
	 * 
	 */
	class albumController extends publicController {
		
		public function itemlist(){
			
			$data = session('_upload_cache');
			//session('_upload_cache',null);
// 			debug(json_decode($data));
			$data = json_decode($data,true);
			if (empty($data)){
				echo "还没有上传图片";
			}else{
				$html = '<ul class="updone">';
				foreach ($data as $key=>$val){
	          		$html .= '<li id="pic_'.$val['id'].'"  class="dlist">';
	              	$html .= '<img views="'.$val['folder'].'/resize_'.$val['savename'].'" src="'.$val['folder'].'/resize_'.$val['savename'].'" id="img_'.$val['id'].'" />';
					$html .= '<a href="javascript:;" rel="'.$val['folder']."/resize_".$val['savename'].'" vkey="'.$val['id'].'" class="setThumb" id="link_'.$val['id'].'" title="设置此图片为组图缩略图"></a>';
					$html .= '<a href="javascript:deluppic('.$val['id'].');" rel="'.$val['id'].'" class="deluppic" id="deluppic_'.$val['id'].'" title="删除此图片"></a>';
					$html .= '<input name=pics[] type="hidden" value="'.$val['id'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][keep]" type="hidden" value="'.$val['id'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][folder]" type="hidden" value="'.$val['folder'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][extension]" type="hidden" value="'.$val['extension'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][size]" type="hidden" value="'.$val['size'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][name]" type="hidden" value="'.$val['savename'].'" />';
// 					$html .= '<input name="pics['.$val['id'].'][text]" type="hidden" value="'.$val['name'].'" />';
					$html .= '</li>';
	          		
	            }
				$html .= '<div class="clear"></div>';
// 				$html .= '<li class="delall"><a href="javascript:delalluppic()" class="submit">清空并删除所有已上传图片</a></li>';
				$html .= '</ul>';
				echo $html;
			}
		}

		public function delpic(){
			$id = intval($_GET['id']);
			$data = M('file')->where('id ='.$id)->getOne();
			if(!empty($data)){
				//删除文件
				@unlink(ROOT_PATH.'/'.$data['folder'].$data['savename']);
				@unlink(ROOT_PATH.'/'.$data['folder'].'resize_'.$data['savename']);
				
				//修改session
				$data = session('_upload_cache');
				$data = json_decode($data,true);
				unset($data[$id]);
				session('_upload_cache',json_encode($data));
				
				//删除数据库信息
				M('file')->delete('id='.$id);
				jsonOUT(array('result'=>true));
			}
			
		}
		
		public function upload(){
			$upload = new file();
			$uploadfilepath = 'static/uploads';
			$upload->savePath = $uploadfilepath."/";
			$upload->maxSize = 3145728;
			$upload->allowExts = array('jpg','gif','png','jpeg');
			$upload->saveRule = 'uniqid';
			$upload->autoSub = true;
			$upload->subType = 'date';
			$upload->dateFormat = "Ymd";
			
			//thumb
			$upload->thumb = true;
			$upload->thumbMaxWidth = '160';
			$upload->thumbMaxHeight = '160';
			$upload->thumbPrefix = 'resize_';
			$upload->thumbPath = $uploadfilepath."/".date("Ymd")."/";
			
			$upload->upload();
			$info = $upload->getUploadFileInfo();
			D('file')->saveFileToDB($info);
		}

// 		private function createFixThumb($files,$folder){
// 			$_path = __ROOT_PATH__ . '/' . C( 'WEB_PUBLIC_PATH' ) . '/' . C( 'DIR_UPLOAD_PATH' ) . '/' . $folder;
// 			$_org = $_path . '/' . $files;
// 			$_width = C('PIC_FIX_WIDTH');
// 			$create = $_path . '/fix_' . $files;
// 			if ( !$_width ){
// 				$_width = C('PIC_THUMB_WIDTH');
// 			}
// 			//获取原图信息
// 			$_info = getimagesize($_org);
// 			$_o_width = $_info[0];
// 			$_o_height = $_info[1];

// 			$_n_width = $_width; //--
// 			$_n_height = $_width * $_o_height / $_o_width; //--
			
// 			$_return = '';
// 			if($_info[0] > $_n_width && $_info[1] > $_n_height) {
// 				switch($_info[2]) {
// 					case 1 :
// 						$im = imagecreatefromgif($_org);
// 						break;
// 					case 2 :
// 						$im = imagecreatefromjpeg($_org);
// 						break;
// 					case 3 :
// 						$im = imagecreatefrompng($_org);
// 						break;
// 				}
// 				$ni = imagecreatetruecolor($_n_width, $_n_height);
// 				imagecopyresampled($ni, $im, 0, 0, 0, 0, $_n_width, $_n_height, $_info[0], $_info[1]);
// 				if ( $_info[2] == 2 ){
// 					imagejpeg($ni, $create, 95);
// 				}else{
// 					imagejpeg($ni, $create);
// 				}
// 				imagedestroy($im);
// 				imagedestroy($ni);
// 			}
// 		}

		//
	}
	
?>