<?php
class newsController extends commonController{
	
	
	public function __construct(){
		parent::__construct();
	}
	
	
	public function addNews(){
		if(isPost()){
			$data = $_POST;
			$data['title'] = html_encode($data['title']);
			$data['keywords'] = html_encode($data['keywords']);
			$data['description'] = html_encode($data['description']);
			$data['content'] = html_encode($data['content']);
			$data['createtime'] = $data['updatetime'] = time();
			
			if(M('news')->insert($data)){
				redirect(U('admin/news/newsManage'));
			}
			
		}else{
			$data = $parentCate = $cate = array();
			//新闻分类
			$parentCate = M('category')->where('type = "news" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			
			$data['cate'] = $parentCate;
			$this->assign('data', $data);
			$this->display('addNews.html');
		}
		
	}
	
	public function newsManage(){
		
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('news')->where()->page($pageNum)->order('id DESC')->getAll();
		
		//新闻分类
		$parentCate = M('category')->where('type = "news" AND parentid = 0')->getOne();
		$cate = M('category')->where('parentid = '.$parentCate['id'])->getAll();
		$category = array();
		foreach($cate as $v){
			$category[$v['id']] = $v;
		}
		$cate = $category;
		
		$page = M('news')->getPager($pageNum, 'admin/news/newsManage');
		
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('page', $page);
		
		$this->display('newsManage.html');
	}
	
	public function editNews(){
		$id = $_GET['id'];
		
		if(isPost()){
			$data = array();
			$data['cateid'] = $_POST['cateid'];
			$data['title'] = html_encode($_POST['title']);
			$data['keywords'] = html_encode($_POST['keywords']);
			$data['description'] = html_encode($_POST['description']);
			$data['content'] = html_encode($_POST['content']);
			$data['updatetime'] = time();
			if(M('news')->update('id='.$id,$data)){
				redirect(U('admin/news/newsManage'));
			}
		}else{
			
			$data = M('news')->where('id ='.$id)->getOne();
			
			//新闻分类
			$parentCate = M('category')->where('type = "news" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			$data['title'] = html_decode($data['title']);
			$data['keywords'] = html_decode($data['keywords']);
			$data['description'] = html_decode($data['description']);
			$data['content'] = html_decode($data['content']);
			$data['content'] = html_decode($data['content']);
			
			$this->assign('cate', $parentCate);
			$this->assign('data', $data);
			
			$this->display('editNews.html');
		}
		
	}
	
	public function preview(){
		
		$id = $_GET['id'];
		$data = M('news')->where('id ='.$id)->getOne();
		
		//新闻分类
		$cate = M('category')->where('id = '.$data['cateid'])->getOne();
		$data['cateName'] = $cate['categoryname'];
		$data['content'] = stripslashes(htmlspecialchars_decode($data['content']));
// 		debug($data);
		$this->assign('data', $data);
		$this->display('preview.html');
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('news')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
}