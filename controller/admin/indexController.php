<?php
class indexController extends  commonController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->assign('username', session('username'));
		$this->display('index.html');
	}
	// 显示管理后台头部内容
	public function header(){
		$this->display('header.html');
	}
	
	// 显示管理后台左侧菜单内容
	public function left(){
		$this->display('left.html');
	}
	
	// 显示管理后台欢迎页
	public function home(){
		$this->display('home.html');
	}
	
}