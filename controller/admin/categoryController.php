<?php
class categoryController extends commonController{
	
	private $type = array();
	
	public function __construct(){
		parent::__construct();
		
		$this->type = config('navtype');
	}
	
	public function categoryManage(){
		$data = array();
		
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('category')->where()->page($pageNum)->getAll();
		$page = M('category')->getPager($pageNum, 'admin/category/categoryManage');
		
		$this->assign('data', $data);
		$this->assign('page', $page);
		$this->display('categoryManage.html');
	}
	
	public function addCategory(){
		
		if(isPost()){
			$data = $_POST;
			$data['categoryname'] = html_encode($data['categoryname']);
			M('category')->insert($data);
			redirect(U('admin/category/categoryManage'));
		}else{
			$data = array();
			$data['category'] = M('category')->where('parentid = 0 ')->getAll();
			
			$this->type = M('mod')->field('`mod`,`name`')->where('issystem = 1')->getAll();
			
			
			$this->assign('type', $this->type);
			$this->assign('data', $data);
			$this->display('addCategory.html');
		}
	}
	
	public function editCategory(){
		
		$id = intval($_GET['id']);
		if(isPost()){
			$data = $_POST;
			$data['categoryname'] = html_encode($data['categoryname']);
			if(M('category')->update('id='.$id,$data)){
				redirect(U('admin/category/categoryManage'));
			}else{
				redirect(U('admin/category/categoryManage',array('id'=>$id)));
			}
		}else{
			$data = M('category')->where('id ='.$id)->getOne();
			$data['categoryname'] = html_decode($data['categoryname']);
			
			$data['category'] = M('category')->where('parentid = 0 ')->getAll();
			
// 			debug($data);
			$this->type = M('mod')->field('`mod`,`name`')->where('issystem = 1')->getAll();
			
			$this->assign('type', $this->type);
			$this->assign('data', $data);
				
			$this->display('editCategory.html');
			
		}
		
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('category')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
}