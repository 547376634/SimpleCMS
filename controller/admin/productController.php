<?php
class productController extends commonController{
	
	public function __construct(){
		parent::__construct();
	}
	
	
	public function addProduct(){
		if(isPost()){
			$data = $_POST;
			$data['name'] = html_encode($_POST['name']);
			$data['keywords'] = html_encode($_POST['keywords']);
			$data['brand'] = html_encode($_POST['brand']);
			$data['model'] = html_encode($_POST['model']);
			$data['color'] = html_encode($_POST['color']);
			$data['unit'] = html_encode($_POST['unit']);
			$data['description'] = html_encode($_POST['description']);
			$data['pics'] = implode(',', $_POST['pics']);
			$data['picindex'] = intval($_POST['picindex']);
			$data['createtime'] = $data['updatetime'] = time();
			if(M('product')->insert($data)){
				redirect(U('admin/product/productManage'));
			}else{
				redirect(U('admin/product/addProduct'));
			}
			
		}else{
			//新闻分类
			$parentCate = M('category')->where('type = "product" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			session('_upload_cache',null);//清空
			$this->assign('cate', $parentCate);
			$this->display('addProduct.html');
		}
	}
	
	public function editProduct(){
		$id = $_GET['id'];
		
		if(isPost()){
			$data = array();
			$data = $_POST;
			$data['name'] = html_encode($_POST['name']);
			$data['keywords'] = html_encode($_POST['keywords']);
			$data['brand'] = html_encode($_POST['brand']);
			$data['model'] = html_encode($_POST['model']);
			$data['color'] = html_encode($_POST['color']);
			$data['unit'] = html_encode($_POST['unit']);
			$data['description'] = html_encode($_POST['description']);
			$data['updatetime'] = time();
			$data['pics'] = implode(',', $_POST['pics']);
			$data['picindex'] = intval($_POST['picindex']);
			
			if(M('product')->update('id='.$id,$data)){
				redirect(U('admin/product/productManage'));
			}
		}else{
				
			$data = M('product')->where('id ='.$id)->getOne();
				
			//新闻分类
			$parentCate = M('category')->where('type = "product" AND parentid = 0')->getAll();
				
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			$data['name'] = html_decode($data['name']);
			$data['keywords'] = html_decode($data['keywords']);
			$data['brand'] = html_decode($data['brand']);
			$data['model'] = html_decode($data['model']);
			$data['color'] = html_decode($data['color']);
			$data['unit'] = html_decode($data['unit']);
			$data['description'] = html_decode($data['description']);
			$data['pics'] = D('file')->getPicList($data['pics']);

			session('_upload_cache',json_encode($data['pics']));
				
			$this->assign('cate', $parentCate);
			$this->assign('data', $data);
				
			$this->display('editProduct.html');
		}
	}
	
	public function productManage(){
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('product')->where()->page($pageNum)->order('id DESC')->getAll();
		
		//产品分类
		$parentCate = M('category')->where('type = "product" AND parentid = 0')->getOne();
		$cate = M('category')->where('parentid = '.$parentCate['id'])->getAll();
		$category = array();
		foreach($cate as $v){
			$category[$v['id']] = $v;
		}
		$cate = $category;
		
		$page = M('product')->getPager($pageNum, 'admin/product/productManage');
		
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('page', $page);
		
		$this->display('productManage.html');
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('product')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
	
}