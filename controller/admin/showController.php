<?php
class showController extends commonController{
	
	
	public function __construct(){
		parent::__construct();
	}
	
	public function addShow(){
		
		if(isPost()){
			$data = array();
			$data['title'] = html_encode($_POST['title']);
			$data['content'] = html_encode($_POST['content'] );
			$data['pics'] = implode(',', $_POST['pics']);
			$data['picindex'] = intval($_POST['picindex']);
			$data['cateid'] = intval($_POST['cateid']);
			$data['createtime'] = time();
			
			$id = M('show')->insert($data);
			redirect(U('admin/show/showManage'));
		}else{
			//新闻分类
			$parentCate = M('category')->where('type = "show" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			session('_upload_cache',null);//清空
			$this->assign('cate', $parentCate);
			$this->display('addShow.html');
		}
		
		
	}
	
	public function showManage(){
		
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('show')->where()->page($pageNum)->getAll();

		$page = M('show')->getPager($pageNum, 'admin/show/showManage');
		
		$this->assign('data', $data);
		$this->assign('page', $page);
		
		
		$this->display('showManage.html');
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('show')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
	
	public function editShow(){
		$id = $_GET['id'];
		
		if(isPost()){
			$data = array();
			$data['title'] = html_encode($_POST['title']);
			$data['content'] = html_encode($_POST['content']);
			$data['updatetime'] = time();
			$data['pics'] = implode(',', $_POST['pics']);
			$data['picindex'] = intval($_POST['picindex']);
			$data['cateid'] = intval($_POST['cateid']);
			
// 			debug($data);
			
			if(M('show')->update('id='.$id,$data)){
				redirect(U('admin/show/showManage'));
			}
		}else{
			$data = M('show')->where('id ='.$id)->getOne();
			$data['content'] = html_decode($data['content']);
			
			//新闻分类
			$parentCate = M('category')->where('type = "show" AND parentid = 0')->getAll();
				
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			$this->assign('cate', $parentCate);
			
			$this->assign('data', $data);
			$this->display('editShow.html');
		}
		
	}
}