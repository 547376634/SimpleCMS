<?php
class jobController extends publicController{
	
	private $cate = array();
	public function __construct(){
		parent::__construct();
		
	}
	
	public function jobManage(){
		
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('job')->where()->page($pageNum)->order('id DESC')->getAll();
		
		$page = M('job')->getPager($pageNum, 'admin/job/jobManage');
		
		//招聘分类
		$cate = M('category')->where('type = "job"')->getAll();
		$category = array();
		foreach($cate as $v){
			$category[$v['id']] = $v;
		}
		$cate = $category;
		
		$this->assign('data', $data);
		$this->assign('cate', $cate);
		$this->assign('page', $page);
		
		$this->display('jobManage.html');
		
	}
	
	public function addJob(){
		if(isPost()){
			$data = $_POST;
			$data['name'] = html_encode($_POST['name']);
			$data['jobdes'] = html_encode($data['jobdes']);
			$data['createtime'] = $data['updatetime'] = time();
				
			if(M('job')->insert($data)){
				redirect(U('admin/job/jobManage'));
			}
		}else{
			
			//招聘分类
			$parentCate = M('category')->where('type = "job" AND parentid = 0')->getAll();
				
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
// 			debug($parentCate);
			$this->assign('cate', $parentCate);
			
			$this->display('addJob.html');
		}
	}
	
	public function editJob(){
		
		$id = $_GET['id'];
		
		if(isPost()){
			$data = array();
			$data['name'] = html_encode($_POST['name']);
			$data['nums'] = intval($_POST['nums']);
			$data['email'] = $_POST['email'];
			$data['tel'] = $_POST['tel'];
			$data['cateid'] = $_POST['cateid'];
			$data['phone'] = $_POST['phone'];
			$data['jobdes'] = html_encode($_POST['jobdes']);
			$data['updatetime'] = time();
			if(M('job')->update('id='.$id,$data)){
				redirect(U('admin/job/jobManage'));
			}
		}else{
				
			$data = M('job')->where('id ='.$id)->getOne();
				
			$data['name'] = html_decode($data['name']);
			$data['jobdes'] = html_decode($data['jobdes'] );
			
			//招聘分类
			$parentCate = M('category')->where('type = "job" AND parentid = 0')->getAll();
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
				
			$this->assign('cate', $parentCate);
			$this->assign('data', $data);
				
			$this->display('editJob.html');
		}
		
		
		
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('job')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
}