<?php
class pageController extends commonController{
	
	
	public function __construct(){
		parent::__construct();
	}
	
	public function addPage(){
		
		if(isPost()){
			$data = array();
			$data['title'] = html_encode($_POST['title']);
			$data['shortname'] = strip_tags($_POST['shortname']);
			$data['content'] = html_encode($_POST['content'] );
			$data['createtime'] = time();
			
			$id = M('page')->insert($data);
			redirect(U('admin/page/pageManage'));
		}else{
			$this->display('addPage.html');
		}
		
		
	}
	
	public function pageManage(){
		
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		
		$data = M('page')->where()->page($pageNum)->getAll();

		$page = M('page')->getPager($pageNum, 'admin/page/pageManage');
		
		$this->assign('data', $data);
		$this->assign('page', $page);
		
		
		$this->display('pageManage.html');
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('page')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
	
	public function editPage(){
		$id = $_GET['id'];
		
		if(isPost()){
			$data = array();
			$data['title'] = html_encode($_POST['title']);
			$data['shortname'] = strip_tags($_POST['shortname']);
			$data['content'] = html_encode($_POST['content']);
			$data['updatetime'] = time();
			if(M('page')->update('id='.$id,$data)){
				redirect(U('admin/page/pageManage'));
			}
		}else{
			$data = M('page')->where('id ='.$id)->getOne();
			$data['content'] = html_decode($data['content']);
			
			$this->assign('data', $data);
			$this->display('editPage.html');
		}
		
	}
}