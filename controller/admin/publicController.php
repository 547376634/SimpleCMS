<?php
class publicController extends Controller{
	
	
	public function __construct(){
		parent::__construct();
	}
	
	public function login(){
		$this->display('login.html');
	}
	
	
	// 登录检测
	public function checklogin() {
		// 获取数据
		$username = $_POST['username'];
		$password = md5($_POST['password']);
		$verify = $_POST['verify'];
	
		// 数据验证
		if (empty($username)) {
			jsonOUT(array("data"=>array("reason" => '请输入用户名'), "result" => false));
			return;
		}
		if (empty($_POST['password'])) {
			jsonOUT(array("data"=>array("reason" => '请输入密码'), "result" => false));
			return;
		}
		if (empty($_POST['verify'])) {
			jsonOUT(array("data"=>array("reason" => '请输入验证码'), "result" => false));
			return;
		}
		if (strtoupper($verify) != $_SESSION['verify']) {
			jsonOUT(array("data"=>array("reason" => '验证码错误'), "result" => false));
			return;
		}
	
		$model = M("admin");
		$user_info = $model->where("username = '{$username}'")->getOne();
		// 数据库操作
		if ( $user_info ) {
			if( $password == $user_info['password'] ) {
				// 设置登录信息
				session('adminid',$user_info['id']);
				session('username',$user_info['username']);
				session('roleid',$user_info['roleid']);
				session('lastlogintime',$user_info['lastlogintime']);
				 
				// 更新帐号登录信息
// 				$loginip = getClientIp();
// 				$data = array('loginip'=>$loginip,'lastlogintime'=>time(),'logincount'=>$user_info['logincount']+1);
// 				$condition['adminid'] = $user_info['adminid'];
// 				$model->where($condition)->setField($data);
				jsonOUT(array("data"=>array("reason" => '登录成功！'), "result" => true));
			} else {
				jsonOUT(array("data"=>array("reason" => '用户名或密码错误，请重新输入'), "result" => false));
			} // end if password
		} else {
			jsonOUT(array("data"=>array("reason" => '用户名或密码错误，请重新输入'), "result" => false));
		} // end if admin
		return;
	}
	
	public function logout() {
		if(session('adminid')) {
			session('adminid',null);
			session('adminname',null);
			session('adminpower',null);
			session('lastlogintime',null);
			
			//清空上传文件
			session('_upload_cache',null);
		}
		jsonOUT(array('result'=>true,'data'=>array('url'=>U('admin/public/login'))));
	}
	
	
	public function captcha(){
		
		$captcha = load('captcha');
		$captcha->create();
	}
	
	public function clearCache(){
		$arr = config('smarty');
		deldir($arr['compile_dir']);
		deldir($arr['cache_dir']);
	}
}