<?php
class linkController extends commonController{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function addLink(){
		
		if(isPost()){
			$data = $_POST;
			$data['linkname'] = html_encode($data['linkname']);
			$data['createtime'] = $data['updatetime'] = time();
				
			M('link')->insert($data);
			redirect(U('admin/link/linkManage'));
		}else{
			//连接分类
			$parentCate = M('category')->where('type = "link" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			$this->assign('cate', $parentCate);
			$this->display('addLink.html');
		}
	}
	
	
	public function linkManage(){
		
		$data = array();
		$pageNum = empty($_GET['page']) ? 1:intval($_GET['page']);
		$data = M('link')->where()->page($pageNum)->order('id DESC')->getAll();
		
		$page = M('link')->getPager($pageNum, 'admin/link/linkManage');
		
		$this->assign('data', $data);
		$this->assign('page', $page);
		$this->display('linkManage.html');
	}
	
	public function editLink(){
		$id = $_GET['id'];
		if(isPost()){
			$data = $_POST;
			$data['linkname'] = html_encode($data['linkname']);
			$data['updatetime'] = time();
				
			M('link')->update('id ='.$id,$data);
			redirect(U('admin/link/linkManage'));
		}else{
			//连接分类
			$parentCate = M('category')->where('type = "link" AND parentid = 0')->getAll();
			
			foreach($parentCate as &$item){
				$cate = array();
				$cate = M('category')->where('parentid = '.$item['id'])->getAll();
				$item['items'] = $cate;
			}
			
			$data = array();
			$data = M('link')->where('id='.$id)->getOne();
			
			$this->assign('cate', $parentCate);
			$this->assign('data', $data);
			$this->display('editLink.html');
		}
	}
	
	public function del(){
		$id = intval($_GET['id']);
		$data = M('link')->delete('id = '.$id);
		if($data){
			jsonOUT(array('result'=>true,'data'=>$data));
		}else{
			jsonOUT(array('result'=>false, 'data'=>array('reason'=>'执行删除失败')));
		}
	}
	
}